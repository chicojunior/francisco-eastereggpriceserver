var express = require('express');
var api = require('./routes/api');
var app = express();

app.get('/', function (req, res) {
  res.send('Hello World')
});

app.use(api);

app.listen(3000);